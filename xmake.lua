-- =========================
-- Project information
-- =========================
add_rules("mode.debug", "mode.release", "mode.valgrind")

add_repositories("local-repo xmake/repo")

includes("./xmake/scripts/boot-libs.lua")
add_requireconfs("spdlog", { configs = { fmt_external = false }})


add_requires("dl")

set_project("discord-framework")

-- =========================
-- Include tasks and others requirements
-- =========================
includes("./xmake/scripts/tasks.lua")

-- =========================
-- The project core
-- =========================
target("discord-framework")
    -- Project metadata
    set_version("2.1.2")
    -- Project types
    set_kind("binary")
    set_default(true)

    -- Configure "version.h" file
    set_configvar("AUTHOR", "Bricklou")
    set_configvar("PROJECT_NAME", "discord-framework")
    set_configdir("$(buildir)/include", { pattern = "@(.-)@" })
    add_includedirs("$(buildir)/include")
    add_configfiles("src/version.h.in", {
                        pattern = "@(.-)@"
    })

    -- Flags config
    set_languages("cxx20")
    add_cxflags("-fPIC -rdynamic", { force = true })
    add_ldflags("-rdynamic")

    -- Ask to the binary to search libs in the current folder
    -- OR the `lib/` folder
    add_rpathdirs("$ORIGIN", "$ORIGIN/lib")

    if (is_mode("debug")) then
        set_warnings("all", "error", "extra")
        set_symbols("debug")
    end

    -- Dependencies
    add_syslinks("pthread", "pq")
    load_packages()

    -- Sources
    add_files("src/**.cpp")
    add_headerfiles("src/**.h")
    add_includedirs("include/")

    set_installdir("./export")

    before_install(function(target)
            os.rm(target:installdir())
    end)

    after_install(function(target)
            import("core.project.project")
            os.mv(target:installdir().."/bin/"..project.name(), target:installdir())
            os.rm(target:installdir().."/bin")
    end)

    load_autoformat()

    if is_mode("valgrind") then
        on_run(function(target)
            import("core.base.option")
            local args = option.get("arguments") or {}
            local program = target:targetfile()

            import("lib.detect.find_program")

            local p = find_program("valgrind")
            if p ~= nil then
                table.insert(args, 1, program)
                os.execv(p, args)
            else
                print("Valgrind not found")
            end
        end)
    end
