# Discord-Framework

<img src="./resources/logo.png" width="150" height="auto" />

Discord bot framework created in C++ using [D++ library](https://github.com/brainboxdotcpp/DPP). It include a database support (PostgresSQL), a logger, a config manager and a plugin system.

[Documentation](https://discord-framework.gitlab.io/discord-framework/) |
[Loaders](https://gitlab.com/discord-framework/loaders) |
[Plugins](https://gitlab.com/discord-framework/plugins)

## Requirements

You need to install the following softwares on your computer:

- [XMake](https://xmake.io/#/)
- Doxygen (optional, only to generate the documentation)

## Build

To build the Discord bot, execute theses commands in a command line:

``` sh
$ xmake build # -v for verbose
```

And to run the bot:

``` sh
$ xmake run -w . discord-framework
```

## Licence

Check the [LICENCE.md](./LICENCE.md) file

## Contribution

Don't hesitate to make a PR if you see something is missing, wrong or deprecated.
