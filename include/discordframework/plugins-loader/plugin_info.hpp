#pragma once

#include <discordframework/export.hpp>
#include <filesystem>
#include <string>

namespace DiscordFramework {
struct EXPORT_DF PluginInfo {
  std::string name;
  std::string loader;
  std::filesystem::path path;
};

namespace Plugins {

void EXPORT_DF
checkIfDirectoryValid(const std::filesystem::directory_entry &entry);

PluginInfo EXPORT_DF readDirectory(const std::filesystem::path &path);

} // namespace Plugins
} // namespace DiscordFramework
