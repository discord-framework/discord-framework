#include <discordframework/utils/files.hpp>
#include <exception>
#include <fmt/format.h>

namespace Helpers {
std::string Files::downloadFile(const std::string &url) {
  cpr::Response r = cpr::Get(cpr::Url{url});

  if (r.status_code != 200) {
    throw DiscordFramework::http_exception(
        fmt::format("Failed to make request with error code {}: {}",
                    r.status_code, r.error.message));
  }
  return r.text;
}

std::string Files::uploadFile(const std::string &url, const std::string &data) {
  cpr::Response r = cpr::Post(cpr::Url{url}, cpr::Body{data});

  if (r.status_code != 200) {
    throw DiscordFramework::http_exception(
        fmt::format("Failed to make request with error code {}: {}",
                    r.status_code, r.error.message));
  }

  return r.text;
}
} // namespace Helpers
