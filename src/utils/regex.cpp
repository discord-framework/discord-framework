#include <discordframework/utils/regex.hpp>
#include <iostream>

namespace Helpers {
/**
 * Constructor for PCRE regular expression. Takes an expression to match
 * against and optionally a boolean to indicate if the expression should
 * be treated as case sensitive (defaults to false). Constructor compiles
 * the regex, which for a well formed regex may be more expensive than
 * matching against a string.
 *
 * @param the regex to use
 * @parm if true, will be case sensitive
 */
PCRE::PCRE(const std::string &match, bool case_insensitive) {
  compiled_regex = pcre_compile(
      match.c_str(),
      case_insensitive ? PCRE_CASELESS | PCRE_MULTILINE : PCRE_MULTILINE,
      &pcre_error, &pcre_error_ofs, NULL);

  if (!compiled_regex) {
    throw DiscordFramework::regex_exception(pcre_error);
  }
}

/**
 * Match regular expression against a string, returns true on match, false
 * if no match.
 *
 * @param string to compare with
 */
bool PCRE::match(const std::string &comparision) {
  return (pcre_exec(compiled_regex, NULL, comparision.c_str(),
                    comparision.length(), 0, 0, NULL, 0) > -1);
}

/**
 * Match regular expression against a string, and populate a referenced vector
 * with an array of matches, formatted pretty much like PHP's preg_match().
 * Returns true if at least one match was found, false if the string did not
 * match.
 *
 * @param array of string to compare with
 */
bool PCRE::match(const std::string &comparison,
                 std::vector<std::string> &matches) {
  /* Match twice: first to find out how many matches there are, and again to
   * capture them all */
  matches.clear();
  int matcharr[90];
  int matchcount = pcre_exec(compiled_regex, NULL, comparison.c_str(),
                             comparison.length(), 0, 0, matcharr, 90);
  if (matchcount == 0) {
    throw DiscordFramework::regex_exception("Not enough room in matcharr");
  }
  for (int8_t i = 0; i < matchcount; ++i) {
    /* Ugly char ops */
    auto s = std::string(comparison.c_str() + matcharr[2 * i],
                         (size_t)(matcharr[2 * i + 1] - matcharr[2 * i]));
    if (s.length() > 0) {
      matches.push_back(s);
    }
  }
  return matchcount > 0;
}

/**
 * Destructor to free compiled regular expression structure
 */
PCRE::~PCRE() {
  /* Ugh, C libraries */
  free(compiled_regex);
}
}; // namespace Helpers
