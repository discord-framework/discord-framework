#include <discordframework/utils/strings.hpp>

namespace Helpers {
std::string Strings::replaceString(std::string subject,
                                   const std::string &search,
                                   const std::string &replace) {
  size_t pos = 0;

  std::string subject_lc = toLower(subject);
  std::string search_lc = toLower(search);
  std::string replace_lc = toLower(replace);

  while ((pos = subject_lc.find(search_lc, pos)) != std::string::npos) {
    subject.replace(pos, search.length(), replace);
    subject_lc.replace(pos, search_lc.length(), replace_lc);

    pos += replace.length();
  }

  return subject;
}
} // namespace Helpers
