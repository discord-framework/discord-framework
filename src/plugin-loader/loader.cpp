#include <discordframework/plugins-loader/loader.hpp>

namespace DiscordFramework {
Loader::Loader(Bot *bot) : m_bot(bot) {}

Loader::~Loader() {}

void Loader::load(PluginInfo *info) {}
} // namespace DiscordFramework
