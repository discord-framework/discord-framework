#include <discordframework/plugins-loader/loader_manager.hpp>

#include <discordframework/plugins-loader/plugin_info.hpp>

#include <filesystem>
#include <spdlog/spdlog.h>

namespace DiscordFramework {
LoaderManager::LoaderManager() {}

LoaderManager::~LoaderManager() {}

void LoaderManager::init(ServiceManager *service_manager) {
  std::filesystem::path folder_path{"./loaders"};

  // Create the loaders directory if it doesn't exist
  if (!std::filesystem::exists(folder_path)) {
    try {
      std::filesystem::create_directory(folder_path);
    } catch (std::filesystem::filesystem_error &e) {
      SPDLOG_INFO("Error creating loaders directory: {}", e.what());

      std::exit(1);
    }
  }

  // Load all the loaders
  for (auto &entry : std::filesystem::directory_iterator(folder_path)) {
    if (entry.is_regular_file()) {
      std::string filename = entry.path().string();

      try {

        SPDLOG_DEBUG("Loading loader: {}", filename);

        // Load the loader
        this->load(std::data(filename), service_manager);

      } catch (std::exception &e) {
        SPDLOG_WARN("Error loading loaders \"{}\":\n{}", filename, e.what());

        continue;
      }
    } else {
      SPDLOG_WARN("Error loading loaders: \"{}\" is not a file",
                  entry.path().string());
      continue;
    }
  }

  SPDLOG_INFO("{} plugin loaders found", m_loaders.size());
}

void LoaderManager::load(const std::string &path,
                         ServiceManager *service_manager) {
  DLOpen *library = new DLOpen(path);

  auto loader_init = library->load<initfunctype *>("init_loader");

  if (loader_init != nullptr) {
    // Initialize loader
    auto loader = loader_init(service_manager->bot());

    // Add to native list
    this->m_loaders_native.emplace(loader->name(), library);
    // Add the loader to the manager
    this->m_loaders.emplace(loader->name(), loader);
  } else {
    throw std::runtime_error("Error loading loader: init_loader not found");
  }
}

void LoaderManager::load_plugins() {
  std::vector<PluginInfo> plugins_list{};

  std::filesystem::path plugins_folder{"./plugins"};

  if (!std::filesystem::exists(plugins_folder)) {
    std::filesystem::create_directories(plugins_folder);
  }

  auto entries = std::filesystem::directory_iterator(plugins_folder);

  for (const auto &f : entries) {
    try {
      DiscordFramework::Plugins::checkIfDirectoryValid(f);

      auto pInfo = DiscordFramework::Plugins::readDirectory(f.path());

      SPDLOG_DEBUG("Loading plugin \"{}\"", pInfo.name);

      if (m_loaders.contains(pInfo.loader)) {
        plugins_list.push_back(pInfo);
      } else {
        SPDLOG_WARN("Error loading plugin \"{}\": Unknown loader \"{}\"",
                    pInfo.name, pInfo.loader);
        continue;
      }

    } catch (std::exception &e) {
      SPDLOG_WARN("Error loading plugin: {}", e.what());
      continue;
    }
  }

  uint32_t loaded = 0;

  for (auto p : plugins_list) {
    try {
      m_loaders.at(p.loader)->load(&p);
      loaded++;
    } catch (std::exception &e) {
      SPDLOG_WARN("Error loading plugin \"{}\": {}", p.name, e.what());
      continue;
    }
  }

  SPDLOG_INFO("{} plugins loaded", loaded);
}

} // namespace DiscordFramework
