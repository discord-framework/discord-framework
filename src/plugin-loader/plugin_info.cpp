#include <discordframework/plugins-loader/plugin_info.hpp>

#include <fmt/format.h>
#include <fstream>
#include <nlohmann/json-schema.hpp>

using nlohmann::json;
using nlohmann::json_schema::json_validator;

static json plugin_schema = R"(
{
  "$schema": "https://json-schema.org/draft-07/schema#",
  "title": "Discord-Framework plugin manifest",
  "properties": {
    "name": {
      "type": "string",
      "description": "Plugin name"
    },
    "loader": {
      "type": "string",
      "description": "Loader needed to load this plugin"
    },
    "main": {
      "type": "string",
      "description": "Path to the main file"
    }
  },
  "required": ["name", "loader", "main"],
  "type": "object"
}
)"_json;

namespace DiscordFramework::Plugins {
void checkIfDirectoryValid(const std::filesystem::directory_entry &entry) {
  if (!entry.is_directory()) {
    throw std::runtime_error(
        fmt::format("\"{}\" is not a directory", entry.path().string()));
  }

  if (!std::filesystem::exists(entry.path() / "plugin.json")) {
    throw std::runtime_error(
        fmt::format("\"{}\" is not valid. \"plugin.json\" missing.",
                    entry.path().string()));
  }
}

PluginInfo readDirectory(const std::filesystem::path &path) {
  std::string p = (path / "plugin.json").string();
  std::ifstream i(p);

  auto j = json::parse(i, nullptr, true, true);
  i.close();

  json_validator validator;
  validator.set_root_schema(plugin_schema);
  validator.validate(j);

  PluginInfo pInfo{};
  pInfo.name = j["name"];
  pInfo.loader = j["loader"];
  pInfo.path = path / j["main"];

  return pInfo;
}
} // namespace DiscordFramework::Plugins
