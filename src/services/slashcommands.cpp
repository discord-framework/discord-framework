#include <discordframework/services/slashcommands.hpp>

#include <discordframework/core/bot.hpp>

#include <numeric>
#include <spdlog/spdlog.h>

namespace DiscordFramework::Services {
SlashCommandsService::SlashCommandsService() {}

SlashCommandsService::~SlashCommandsService() {}

void SlashCommandsService::boot() {}

void SlashCommandsService::register_command(dpp::slashcommand &cmd) {

  if (!m_commands.contains(cmd.type)) {
    m_commands.insert({cmd.type, {}});
  } else {
    if (cmd.type == dpp::slashcommand_contextmenu_type::ctxm_message &&
        m_commands.at(cmd.type).size() > 5) {
      throw std::out_of_range(
          "Maximum global message commands count reached (5)");
    } else if (cmd.type == dpp::slashcommand_contextmenu_type::ctxm_user &&
               m_commands.at(cmd.type).size() > 5) {
      throw std::out_of_range("Maximum global user commands count reached (5)");
    } else if (m_commands.at(cmd.type).size() > 100) {
      throw std::out_of_range(
          "Maximum global chat commands count reached (100)");
    }
  }

  m_commands[cmd.type].push_back(cmd);
}

void SlashCommandsService::register_command(dpp::snowflake guild_id,
                                            dpp::slashcommand &cmd) {

  if (!m_guild_commands.contains(guild_id)) {
    m_guild_commands.insert({guild_id, {}});
  }

  if (!m_guild_commands[guild_id].contains(cmd.type)) {
    m_guild_commands[guild_id].insert({cmd.type, {}});
  } else {
    if (cmd.type == dpp::slashcommand_contextmenu_type::ctxm_message &&
        m_guild_commands[guild_id][cmd.type].size() > 5) {
      throw std::out_of_range(fmt::format(
          "Maximum message commands count reached for guild \"{}\" (5)",
          guild_id));
    } else if (cmd.type == dpp::slashcommand_contextmenu_type::ctxm_user &&
               m_guild_commands[guild_id][cmd.type].size() > 5) {
      throw std::out_of_range(fmt::format(
          "Maximum user commands count reached for guild \"{}\" (5)",
          guild_id));
    } else if (m_guild_commands[guild_id][cmd.type].size() > 100) {
      throw std::out_of_range(fmt::format(
          "Maximum chat commands count reached for guild \"{}\" (100)",
          guild_id));
    }
  }

  m_guild_commands[guild_id][cmd.type].push_back(cmd);
}

void SlashCommandsService::sync_commands() {
  if (m_service_manager->bot()->cluster->cluster_id == 0) {
    std::vector<dpp::slashcommand> cmd{};

    for (auto &kv : m_commands) {
      cmd.insert(cmd.end(), kv.second.begin(), kv.second.end());
    }

    m_service_manager->bot()->cluster->global_bulk_command_create(
        cmd, std::bind(&SlashCommandsService::on_register_error, this,
                       std::placeholders::_1));

    cmd.clear();

    for (auto &kv : m_guild_commands) {
      for (auto &kv2 : kv.second) {
        cmd.insert(cmd.end(), kv2.second.begin(), kv2.second.end());
      }

      m_service_manager->bot()->cluster->guild_bulk_command_create(
          cmd, kv.first,
          std::bind(&SlashCommandsService::on_register_error, this,
                    std::placeholders::_1));

      SPDLOG_DEBUG("Registered {} commands for \"{}\"", cmd.size(), kv.first);

      cmd.clear();
    }

    SPDLOG_DEBUG("Slashcommands synchronized");
  }
}

void SlashCommandsService::on_register_error(
    const dpp::confirmation_callback_t &callback) {

  if (callback.is_error()) {
    auto error = callback.get_error();
    SPDLOG_ERROR("Error while registering commands: {}", error.message);
  }
}
} // namespace DiscordFramework::Services
