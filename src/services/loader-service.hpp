#pragma once

#include <discordframework/plugins-loader/loader_manager.hpp>
#include <discordframework/services/service.hpp>
#include <memory>
#include <vector>

namespace DiscordFramework::Services {
class LoaderService : public Service {
public:
  /**
   * @brief Initialize the plugin loader service
   *
   * @return The service instance
   */
  LoaderService();

  ~LoaderService();

  /**
   * @brief Boot the plugin loader service
   */
  void boot() override;

  /**
   * @brief Get the loader manager instance
   *
   * @return LoaderManager* The manager
   */
  LoaderManager *loader();

private:
  std::unique_ptr<LoaderManager> m_manager;
};
} // namespace DiscordFramework::Services
