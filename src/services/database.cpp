#include <discordframework/services/database.hpp>

#include <discordframework/services/config.hpp>
#include <spdlog/spdlog.h>

using DiscordFramework::Services::ConfigService;

namespace DiscordFramework::Services {
DatabaseService::DatabaseService() {}

void DatabaseService::boot() {
  auto config = m_service_manager->get<ConfigService>("config");

  auto db_config = config->JSON()->at("database");
  if (db_config.type() != json::value_t::string) {
    SPDLOG_INFO("Database disabled from config");
    return;
  }

  try {
    this->c = std::make_unique<pqxx::connection>(db_config.get<std::string>());
  } catch (std::exception const &e) {
    SPDLOG_ERROR("Unable to connect to database: {}", e.what());
    std::exit(1);
  }
}

DatabaseService::~DatabaseService() {
  if (this->state && c->is_open()) {
    c->close();
  }
}

pqxx::connection *DatabaseService::connection() { return c.get(); }

bool DatabaseService::isEnabled() { return state; }

bool DatabaseService::isConnected() {
  return c.get() != nullptr && c->is_open();
}
} // namespace DiscordFramework::Services
