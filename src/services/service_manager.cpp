#include <discordframework/services/service_manager.hpp>
#include <fmt/format.h>

namespace DiscordFramework {

ServiceManager::ServiceManagerBuilder::ServiceManagerBuilder() {}
ServiceManager::ServiceManagerBuilder::~ServiceManagerBuilder() {}

ServiceManager::ServiceManagerBuilder &
ServiceManager::ServiceManagerBuilder::add_native_service(
    const std::string &name, Service *s) {
  this->m_order.push_back(name);
  this->m_list.emplace(name, s);

  return *this;
}

ServiceManager ServiceManager::ServiceManagerBuilder::build() {
  return ServiceManager{m_list, m_order};
}

ServiceManager::ServiceManagerBuilder ServiceManager::create() {
  return ServiceManagerBuilder{};
}

void ServiceManager::boot() {
  for (auto name : m_native_order) {
    m_native_services.at(name)->boot();
  }
}

ServiceManager::ServiceManager(const ServicesList &s,
                               std::vector<std::string> order)
    : m_native_services{s}, m_native_order{order} {

  for (auto &ns : m_native_services) {
    ns.second->set_manager(this);
  }
}

ServiceManager::ServiceManager(const ServiceManager &s)
    : m_native_services{s.m_native_services}, m_native_order{s.m_native_order} {
}

ServiceManager::~ServiceManager() {
  for (auto it = m_native_order.rbegin(); it != m_native_order.rend(); it++) {
    m_native_services.at(*it)->destroy();
  }
}

ServiceManager &ServiceManager::add(const std::string &name, Service *service) {
  if (this->m_native_services.contains(name)) {
    throw std::logic_error("Native services can't be overrided");
  }

  if (this->m_services.contains(name)) {
    throw std::logic_error(fmt::format("Service \"{}\" already exists", name));
  }

  this->m_services.emplace(name, service);
  return *this;
}

ServiceManager &ServiceManager::remove(const std::string &name) {
  if (this->m_native_services.contains(name)) {
    throw std::logic_error("Native services can't be removed");
  }
  if (!this->m_services.contains(name)) {
    throw std::logic_error(fmt::format("Service \"{}\" doesn't exists", name));
  }
  this->m_services.at(name)->destroy();

  this->m_services.erase(name);
  return *this;
}

bool ServiceManager::has(const std::string &name) {
  return this->m_services.contains(name);
}

Bot *ServiceManager::bot() const { return this->m_bot; }

Service *ServiceManager::getService(const std::string &name) {
  Service *s = nullptr;
  if (this->m_native_services.contains(name)) {
    s = this->m_native_services.at(name);
  } else if (this->m_services.contains(name)) {
    s = this->m_services.at(name);
  } else {
    throw std::logic_error(fmt::format("Service \"{}\" doesn't exists", name));
  }
  return s;
}
} // namespace DiscordFramework
