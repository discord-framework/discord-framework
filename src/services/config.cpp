#include <discordframework/services/config.hpp>

namespace DiscordFramework::Services {

ConfigService::ConfigService(const std::string &path) {
  this->m_config = std::make_unique<DiscordFramework::Configuration>(path);
}

} // namespace DiscordFramework::Services
