#include "./loader-service.hpp"
#include <dlopen.hpp>

namespace DiscordFramework::Services {
LoaderService::LoaderService() {}

void LoaderService::boot() {

  m_manager = std::make_unique<LoaderManager>();

  // Initializing all loaders
  m_manager->init(m_service_manager);

  // Load plugins
  m_manager->load_plugins();
}

LoaderService::~LoaderService() {}

LoaderManager *LoaderService::loader() { return m_manager.get(); }

} // namespace DiscordFramework::Services
