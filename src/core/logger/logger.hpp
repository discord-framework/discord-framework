#pragma once

#include <string>

#include <spdlog/spdlog.h>

namespace DiscordFramework::Logger {
void create_logger(const std::string &name);
}