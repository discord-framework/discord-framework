#include <discordframework/core/bot.hpp>
#include <discordframework/services/slashcommands.hpp>
#include <fmt/format.h>
#include <spdlog/spdlog.h>
#include <version.h>

namespace DiscordFramework {

Bot::Bot(ServiceManager *s) {
  m_service_manager = s;
  s->m_bot = this;

  this->cluster = std::make_unique<dpp::cluster>("");
}

Bot::~Bot() {}

void Bot::start() {
  if (m_started) {
    return;
  }

  this->cluster->token = this->m_token;
  this->cluster->intents = this->m_intents;
  this->cluster->cluster_id = this->m_clusterID;
  this->cluster->maxclusters = this->m_maxClusters;
  this->cluster->numshards = this->m_maxShards;

  this->cluster->on_log([](const dpp::log_t &event) {
    switch (event.severity) {
    case dpp::ll_trace:
      SPDLOG_TRACE("{}", event.message);
      break;
    case dpp::ll_debug:
      SPDLOG_DEBUG("{}", event.message);
      break;
    case dpp::ll_info:
      SPDLOG_INFO("{}", event.message);
      break;
    case dpp::ll_warning:
      SPDLOG_WARN("{}", event.message);
      break;
    case dpp::ll_error:
      SPDLOG_ERROR("{}", event.message);
      break;
    case dpp::ll_critical:
    default:
      SPDLOG_CRITICAL("{}", event.message);
      break;
    }
  });

  this->cluster->on_ready(
      std::bind(&Bot::onReady, this, std::placeholders::_1));

  this->cluster->log(dpp::ll_info, "Starting bot");

  try {
    this->cluster->start(false);
  } catch (const std::exception &e) {
    this->cluster->log(dpp::ll_error, e.what());
    std::exit(0);
  }
}

Bot &Bot::set_token(const std::string &token) {
  if (!m_started) {
    m_token = token;
  }

  return *this;
}

Bot &Bot::set_cluster(uint8_t maxShards, uint8_t clusterID,
                      uint8_t maxClusters) {
  if (!m_started) {
    m_maxShards = maxShards;
    m_clusterID = clusterID;
    m_maxClusters = maxClusters;
  }

  return *this;
}

Bot &Bot::enable_intents(BotIntents intents) {
  if (!m_started) {
    if (intents & BotIntents::member) {
      m_intents |= dpp::intents::i_guild_members;
    }
    if (intents & BotIntents::message) {
      m_intents |= dpp::intents::i_guild_messages | dpp::i_message_content;
    }
    if (intents & BotIntents::presence) {
      m_intents |= dpp::intents::i_guild_presences;
    }
  }

  return *this;
}

dpp::snowflake Bot::getID() const { return this->m_user.id; }
uint32_t Bot::getMaxClusters() const { return this->cluster->maxclusters; }
uint32_t Bot::getClusterID() const { return this->m_clusterID; }
void Bot::setClusterID(uint32_t c) { this->m_clusterID = c; }

std::string Bot::getName() { return PROJECT_NAME; }
std::string Bot::getVersion() { return PROJECT_VERSION; }
std::string Bot::getAuthor() { return PROJECT_AUTHOR; }

void Bot::onReady(const dpp::ready_t &ready) {
  this->m_user = cluster->me;
  m_started = true;

  // Increase the shard init counter
  shard_init_count++;

  cluster->log(
      dpp::ll_debug,
      fmt::format("onReady({}/{})", shard_init_count,
                  cluster->numshards /
                      (cluster->maxclusters ? cluster->maxclusters : 1)));

  m_service_manager
      ->get<DiscordFramework::Services::SlashCommandsService>("slashcommands")
      ->sync_commands();
}

} // namespace DiscordFramework
