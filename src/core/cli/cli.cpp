#include "cli.hpp"

#include <cstdlib>
#include <iostream>

namespace DiscordFramework {
Cli::Cli(int argc, char *argv[]) {
  args::ArgumentParser parser("Discord-Framework");
  args::HelpFlag help(parser, "help", "Display this help menu", {'h', "help"});
  args::CompletionFlag completion(parser, {"complete"});

  args::Group intentsGroup(parser, "Discord bot intents",
                           args::Group::Validators::DontCare);
  args::Flag member(intentsGroup, "member",
                    "Enable the member intent on the bot",
                    {'m', "member-intent"});
  args::Flag message(intentsGroup, "message",
                     "Enable the message intent on the bot",
                     {'M', "message-intent"});
  args::Flag presence(intentsGroup, "presence",
                      "Enable the presence intent on the bot",
                      {'p', "presence-intent"});

  args::Group botConfigGroup(parser, "Discord bot config",
                             args::Group::Validators::DontCare);
  args::ValueFlag<std::string> token(botConfigGroup, "token",
                                     "Set the bot token", {'t', "token"});
  args::ValueFlag<std::string> configPath(botConfigGroup, "config",
                                          "Override the default config path",
                                          {'c', "config"});

  args::Group clusterGroup(parser, "Bot instances management",
                           args::Group::Validators::AllOrNone);
  args::ValueFlag<int> clusterID(clusterGroup, "clusterID",
                                 "Set the cluster ID", {"cluster-id"});
  args::ValueFlag<int> maxClusters(clusterGroup, "maxClusters",
                                   "Set the maximum running clusters",
                                   {"max-clusters"});
  args::ValueFlag<int> shardsPerClusters(
      clusterGroup, "shardsPerClusters",
      "Set the maximum shards running per cluster", {"max-shards"});

  try {
    parser.ParseCLI(argc, argv);
  } catch (const args::Completion &e) {
    std::cout << e.what();
    std::exit(0);
  } catch (const args::Help &) {
    std::cout << parser;
    std::exit(0);
  } catch (const args::ParseError &e) {
    std::cerr << e.what() << std::endl;
    std::cerr << parser;
    std::exit(1);
  }

  if (member)
    this->args.member_intent = args::get(member);
  if (message)
    this->args.message_intent = args::get(message);
  if (presence)
    this->args.presence_intent = args::get(presence);

  if (token)
    this->args.token = args::get(token);
  if (configPath)
    this->args.configPath = args::get(configPath);

  if (clusterID)
    this->args.clusterID = args::get(clusterID);
  if (maxClusters)
    this->args.maxClusters = args::get(maxClusters);
  if (shardsPerClusters)
    this->args.shardsPerClusters = args::get(shardsPerClusters);
}
} // namespace DiscordFramework
