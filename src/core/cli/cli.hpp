#pragma once

#include <args.hxx>
#include <string>

namespace DiscordFramework {

/**
 * @brief Parsed values from the command line
 *
 */
struct Arguments {
  bool member_intent = false;
  bool message_intent = false;
  bool presence_intent = false;
  std::string token = "";
  int clusterID = 0;
  int maxClusters = 1;
  int shardsPerClusters = 0;
  std::string configPath = "./tmp/config.json";
};

/**
 * @brief This class will handle all options and arguments parsing
 * This way, the bot will be able to take information from command line
 */
class Cli {
public:
  /**
   * Construct the Cli object
   * @param argc The argument count
   * @param argv All passed arguments
   */
  Cli(int argc, char *argv[]);
  ~Cli() = default;

  /**
   * @brief Get the parsed arguments
   *
   * @return Arguments The parsed arguments
   */
  Arguments getArgs() const { return this->args; }

private:
  /**
   * @brief To manage properly manage all our argument, we're saving them in one
   * single structure object
   */
  Arguments args{};
};

} // namespace DiscordFramework
