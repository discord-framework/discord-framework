#pragma once

#include "version.h"
#include <iostream>

namespace DiscordFramework {

/**
 * @brief Show a banner with project name, version and author.
 */
void print_boot_message() {
  // Show a nice banner
  std::cout << "\e[0;36m"
            << R"(
 ____  _                       _ _____                                            _
|  _ \(_)___  ___ ___  _ __ __| |  ____ __ __ _ _ __ ___   _____      _____  _ __| | __
| | | | / __|/ __/ _ \| '__/ _` | |_ | '__/ _` | '_ ` _ \ / _ \ \ /\ / / _ \| '__| |/ /
| |_| | \__ | (_| (_) | | | (_| |  _|| | | (_| | | | | | |  __/\ V  V | (_) | |  |   <
|____/|_|___/\___\___/|_|  \__,_|_|  |_|  \__,_|_| |_| |_|\___| \_/\_/ \___/|_|  |_|\_\
)" << std::endl;

  std::cout << "Version: " << PROJECT_VERSION << std::endl;
  std::cout << "Author: " << PROJECT_AUTHOR << std::endl;
  std::cout << "\e[0;0m" << std::endl;
}

} // namespace DiscordFramework
