#include <discordframework/core/configuration.hpp>

#include <fstream>

namespace DiscordFramework {

Configuration::Configuration(const std::string &filePath, json default_config) {
  // Get an absolute path to the configuration file
  config_path = std::filesystem::path(filePath);

  if (config_path.is_relative()) {
    config_path = std::filesystem::absolute(config_path);
  }

  // If the config file doesn't exists, create it and write the default config
  // otherwise, read the config file
  if (!std::filesystem::exists(config_path)) {
    // Create parent directories
    std::filesystem::create_directories(config_path.parent_path());
  } else {
    this->load();
  }

  // If the config is empty, just initialize it with the default one
  if (config.empty()) {
    config = default_config;
    this->save();
  }
}

void Configuration::load() {
  std::ifstream i(this->config_path);
  this->set(i);
  i.close();
}

void Configuration::save() {
  // The code will save the configuration with some code pretty print
  if (!this->JSON()->empty()) {
    std::ofstream o(this->config_path.string());
    o << std::setw(4) << *this->JSON() << std::endl;
    o.close();
  }
}

void Configuration::set(std::ifstream &stream) {
  config = json::parse(stream, nullptr, true, true);
}

bool Configuration::hasEnv(const std::string &key) {
  return std::getenv(key.c_str()) != nullptr;
}
} // namespace DiscordFramework
