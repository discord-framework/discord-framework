#pragma once

#include "../cli/cli.hpp"
#include <nlohmann/json-schema.hpp>

using nlohmann::json;

static json config_schema = R"(
{
  "$schema": "https://json-schema.org/draft-07/schema#",
  "title": "Discord-Framework Configuration",
  "properties": {
    "token": {
      "type": "string",
      "description": "The bot token to use for the bot."
    },
    "database": {
      "description": "Database URI to connect to.",
      "anyOf": [
        {
          "enum": [false]
        },
        {
          "type": "string"
        }
      ]
    }
  },
  "required": ["token", "database"],
  "type": "object"
}
)"_json;

namespace DiscordFramework {
/**
 * @brief Validate a json file against a json schema.
 *
 * @param args Arguments provided by the CLI
 * @param config The configuration that will be validated
 */
void validate_config(Arguments args, json *config);
} // namespace DiscordFramework