#include "validation.hpp"

using nlohmann::json_schema::json_validator;

namespace DiscordFramework {
void validate_config(Arguments args, json *config) {
  // Initialize the validator
  json_validator validator;

  // Ignore the token requirement if it has been provided from the CLI
  if (!args.token.empty()) {
    config_schema["required"].erase("token");
  }

  // Load the schema
  validator.set_root_schema(config_schema);
  // Validate the configuration
  validator.validate(*config);
}
} // namespace DiscordFramework