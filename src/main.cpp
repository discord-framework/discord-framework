#include "core/cli/boot.hpp"
#include "core/cli/cli.hpp"

#include "./services/loader-service.hpp"
#include <discordframework/core/bot.hpp>
#include <discordframework/core/configuration.hpp>
#include <discordframework/services/config.hpp>
#include <discordframework/services/database.hpp>
#include <discordframework/services/service_manager.hpp>
#include <discordframework/services/slashcommands.hpp>
#include <spdlog/spdlog.h>

#include <signal.h>

#include "core/config/validation.hpp"

using namespace DiscordFramework;

/**
 * @brief Exit signal handler
 *
 * @param sig_num
 */
void signal_handler(int sig_num) {
  spdlog::drop_all();
  spdlog::shutdown();

  exit(sig_num);
}

/**
 * @brief Register all exit signal for a smooth exit
 *
 */
void register_signals() {
  // Registering exit signals
  signal(SIGINT, signal_handler);
  signal(SIGABRT, signal_handler);
  signal(SIGTERM, signal_handler);
  signal(SIGHUP, signal_handler);
}

// Program entry point
int main(int argc, char *argv[]) {

  // Let's show a pretty message
  DiscordFramework::print_boot_message();

  // Parse command line arguments, will be usefull to provide custom arguments
  auto cli = std::make_unique<DiscordFramework::Cli>(argc, argv);

  register_signals();

  auto config_service =
      std::make_unique<Services::ConfigService>(cli->getArgs().configPath);
  auto database_service = std::make_unique<Services::DatabaseService>();
  auto loader_service = std::make_unique<Services::LoaderService>();
  auto slashcommands_service =
      std::make_unique<Services::SlashCommandsService>();

  // Load all services
  auto services_mgr =
      DiscordFramework::ServiceManager::create()
          /*
           * Prepare native services
           */
          // Load the configuration service
          .add_native_service("config", config_service.get())
          // Load the database service
          .add_native_service("database", database_service.get())
          // Load the loader service
          .add_native_service("loader", loader_service.get())
          .add_native_service("slashcommands", slashcommands_service.get())
          .build();

  // Create the bot client instance
  auto bot = std::make_unique<DiscordFramework::Bot>(&services_mgr);

  // Boot services
  services_mgr.boot();

  try {
    /*
     * Check that the provided JSON config is valid
     * Otherwise, an error is throwned and the program exit
     */
    DiscordFramework::validate_config(
        cli->getArgs(),
        services_mgr.get<DiscordFramework::Services::ConfigService>("config")
            ->JSON());
  } catch (const std::exception &e) {
    std::cerr << "Failed to validate configuration: \n"
              << e.what() << std::endl;
    return 1;
  }

  /**
   * Configure all necessary to boot properly
   */

  // Set the bot token
  // If specified from argument choose it first instead of the one from config
  std::string token = "";

  if (std::getenv("DF_TOKEN") != nullptr) {
    SPDLOG_INFO("Using token from environment variables");
    token = std::getenv("DF_TOKEN");
  } else {
    if (!cli->getArgs().token.empty()) {
      token = cli->getArgs().token;
    } else {
      token =
          services_mgr.get<DiscordFramework::Services::ConfigService>("config")
              ->JSON()
              ->at("token")
              .get<std::string>();
    }
  }

  bot->set_token(token);

  if (cli->getArgs().maxClusters < 1) {
    SPDLOG_ERROR("Max Clusters count need to be greater than 1.");
    return 1;
  }

  if (cli->getArgs().clusterID < 0 ||
      cli->getArgs().clusterID > cli->getArgs().maxClusters) {
    SPDLOG_ERROR("Invalid cluster ID (0 to {} allowed)",
                 cli->getArgs().maxClusters - 1);
    return 1;
  }

  if (cli->getArgs().shardsPerClusters > 16 ||
      cli->getArgs().shardsPerClusters < 0) {
    SPDLOG_ERROR("Invalid shards count (1 to 16 allowed, 0 to let the Discord "
                 "API decide)");
    return 1;
  }

  // Configure shards
  bot->set_cluster(cli->getArgs().shardsPerClusters, cli->getArgs().clusterID,
                   cli->getArgs().maxClusters);

  // Enable required intents
  if (cli->getArgs().member_intent) {
    bot->enable_intents(DiscordFramework::BotIntents::member);
  }
  if (cli->getArgs().message_intent) {
    bot->enable_intents(DiscordFramework::BotIntents::message);
  }
  if (cli->getArgs().presence_intent) {
    bot->enable_intents(DiscordFramework::BotIntents::presence);
  }

  // Start the bot
  bot->start();

  return 0;
}
